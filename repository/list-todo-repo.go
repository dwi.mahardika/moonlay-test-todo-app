package repository

import (
	"log"

	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/models"

	"github.com/jinzhu/gorm"
)

type todoRepository struct {
	db *gorm.DB
}

type TodoRepo interface {
	SearchListTodo(keyword string, item_per_page, offset int) ([]models.ListTodo, error)
	CountListTodo(keyword string, item_per_page, offset int) (int64, error)
	GetDetailListTodo(id string) (*models.ListTodo, error)
	SearchSubListTodo(id, keyword string, item_per_page, offset int) ([]models.SublistTodo, error)
	CountSubListTodo(id, keyword string, item_per_page, offset int) (int64, error)
	GetDetailSubListTodo(id string) (*models.SublistTodo, error)
	CreateListTodo(req *models.ListTodo) error
	UpdateListTodo(req *models.ListTodo) error
	DeleteListTodo(req *models.ListTodo) error
	CreateSublistTodo(req *models.SublistTodo) error
	UpdateSublistTodo(req *models.SublistTodo) error
	DeleteSublistTodo(req *models.SublistTodo) error
}

func NewTodoRepository(db *gorm.DB) TodoRepo {
	return &todoRepository{db: db}
}

func (r *todoRepository) SearchListTodo(keyword string, item_per_page, offset int) ([]models.ListTodo, error) {
	var listTodos []models.ListTodo
	err := r.db.Preload("SublistTodos").
		Where("LOWER(list_todo.title) LIKE ?", "%"+keyword+"%").
		Where("LOWER(list_todo.description) LIKE ?", "%"+keyword+"%").
		Offset(offset).
		Limit(item_per_page).
		Find(&listTodos).Error
	if err != nil {
		log.Println("failed to search list_todo, err: ", err.Error())
		return nil, err
	}

	return listTodos, nil
}

func (r *todoRepository) CountListTodo(keyword string, item_per_page, offset int) (int64, error) {
	var count int64
	err := r.db.Model(&models.ListTodo{}).
		Where("LOWER(list_todo.title) LIKE ?", "%"+keyword+"%").
		Where("LOWER(list_todo.description) LIKE ?", "%"+keyword+"%").
		Count(&count).Error
	if err != nil {
		log.Println("failed to search list_todo, err: ", err.Error())
		return count, err
	}

	return count, nil
}

func (r *todoRepository) GetDetailListTodo(id string) (*models.ListTodo, error) {
	var listTodos models.ListTodo
	err := r.db.Preload("SublistTodos").
		First(&listTodos, id).Error
	if err != nil {
		log.Println("failed to get detail list_todo, err: ", err.Error())
		return nil, err
	}

	return &listTodos, nil
}

func (r *todoRepository) SearchSubListTodo(id, keyword string, item_per_page, offset int) ([]models.SublistTodo, error) {
	var sublistTodos []models.SublistTodo
	err := r.db.Where("list_id = ?", id).
		Where("LOWER(sublist_todo.title) LIKE ?", "%"+keyword+"%").
		Where("LOWER(sublist_todo.description) LIKE ?", "%"+keyword+"%").
		Offset(offset).
		Limit(item_per_page).
		Find(&sublistTodos).Error
	if err != nil {
		log.Println("failed to search sublist_todo, err: ", err.Error())
		return nil, err
	}

	return sublistTodos, nil
}

func (r *todoRepository) CountSubListTodo(id, keyword string, item_per_page, offset int) (int64, error) {
	var count int64
	err := r.db.Model(&models.SublistTodo{}).
		Where("list_id = ?", id).
		Where("LOWER(sublist_todo.title) LIKE ?", "%"+keyword+"%").
		Where("LOWER(sublist_todo.description) LIKE ?", "%"+keyword+"%").
		Count(&count).Error
	if err != nil {
		log.Println("failed to search list_todo, err: ", err.Error())
		return count, err
	}

	return count, nil
}

func (r *todoRepository) GetDetailSubListTodo(id string) (*models.SublistTodo, error) {
	var sublistTodo models.SublistTodo
	err := r.db.First(&sublistTodo, id).Error
	if err != nil {
		log.Println("failed to get detail sublist_todo, err: ", err.Error())
		return nil, err
	}
	return &sublistTodo, nil
}

func (r *todoRepository) CreateListTodo(req *models.ListTodo) error {
	err := r.db.Create(req).Error
	if err != nil {
		log.Println("failed to create list todo, err: ", err.Error())
		return err
	}
	return nil
}

func (r *todoRepository) UpdateListTodo(req *models.ListTodo) error {
	err := r.db.Save(req).Error
	if err != nil {
		log.Println("failed to update list todo, err: ", err.Error())
		return err
	}
	return nil
}

func (r *todoRepository) DeleteListTodo(req *models.ListTodo) error {
	err := r.db.Delete(req).Error
	if err != nil {
		log.Println("failed to delete list todo, err: ", err.Error())
		return err
	}
	return nil
}

func (r *todoRepository) CreateSublistTodo(req *models.SublistTodo) error {
	err := r.db.Create(req).Error
	if err != nil {
		log.Println("failed to create sublist todo, err: ", err.Error())
		return err
	}
	return nil
}

func (r *todoRepository) UpdateSublistTodo(req *models.SublistTodo) error {
	err := r.db.Save(req).Error
	if err != nil {
		log.Println("failed to update sublist todo, err: ", err.Error())
		return err
	}
	return nil
}

func (r *todoRepository) DeleteSublistTodo(req *models.SublistTodo) error {
	err := r.db.Delete(req).Error
	if err != nil {
		log.Println("failed to delete sublist todo, err: ", err.Error())
		return err
	}
	return nil
}
