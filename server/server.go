package server

import (
	"log"
	"os"

	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/core"
	dbConf "gitlab.com/dwi.mahardika/moonlay-test-todo-app/db"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/handlers"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/repository"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo/v4"
)

type httpServer struct {
	handler *echo.Echo
	close   func()
}

type Server interface {
	Serve(addr string)
	Close()
}

func NewHTTPServer() httpServer {
	dbConf.InitConfig()
	db, err := gorm.Open(os.Getenv("DB_TYPE"), dbConf.DatabaseDSN())
	if err != nil {
		panic("Failed to connect to the database")
	}
	db.LogMode(true)
	dbConf.InitMigration(db.DB())

	repo := repository.NewTodoRepository(db)
	todoListSvc := core.NewToDoService(repo)
	handler := handlers.NewTodoHandler(todoListSvc)
	e := echo.New()

	handlers.RegisterHandler(e, handler)

	return httpServer{
		handler: e,
		close: func() {
			db.Close()
			e.Close()
		},
	}
}

func (h *httpServer) Serve(addr string) {
	log.Println("Server started at", addr)
	err := h.handler.Start(addr)
	if err != nil {
		log.Println("Failed to start server, err: ", err.Error())
	}
	defer h.handler.Close()
}

func (h *httpServer) Close() {
	h.close()
}
