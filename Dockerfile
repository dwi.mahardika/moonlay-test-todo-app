FROM gcr.io/distroless/static

ADD moonlay-test-todo-app ./
COPY db/migrations ./db/migrations
COPY .env ./
ENTRYPOINT ["./moonlay-test-todo-app"]
EXPOSE 8080