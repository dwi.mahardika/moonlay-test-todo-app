package db

import (
	"database/sql"
	"log"
	"os"

	"github.com/pressly/goose"
)

type Config struct {
	DBHost     string
	DBPort     string
	DBUser     string
	DBPassword string
	DBName     string
	DBType     string
}

var config *Config

func InitConfig() {
	config = &Config{
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBUser:     os.Getenv("DB_USER"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBName:     os.Getenv("DB_NAME"),
		DBType:     os.Getenv("DB_TYPE"),
	}
	log.Println("DB Config: ", config)
}

func DatabaseDSN() string {
	switch config.DBType {
	case "postgres":
		return "postgres://" + config.DBUser + ":" + config.DBPassword + "@" + config.DBHost + ":" + config.DBPort + "/" + config.DBName + "?sslmode=disable"
	case "mysql":
		return config.DBUser + ":" + config.DBPassword + "@tcp(" + config.DBHost + ":" + config.DBPort + ")/" + config.DBName
	default:
		panic("Unsupported database type")
	}
}

func InitMigration(db *sql.DB) {
	err := goose.Up(db, "db/migrations")
	if err != nil {
		log.Println("failed to migrate, err: ", err.Error())
		goose.Down(db, "db/migrations")
	}
}
