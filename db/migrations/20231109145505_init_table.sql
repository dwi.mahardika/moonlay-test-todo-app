-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS list_todo (
	id serial8,
	title VARCHAR(100) NOT NULL,
	description TEXT NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	deleted_at TIMESTAMP,
	PRIMARY KEY (id)
);
		
CREATE TABLE IF NOT EXISTS sublist_todo (
	id serial8,
	list_id int8 NOT NULL REFERENCES list_todo (id),
	title VARCHAR(100) NOT NULL,
	description TEXT NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	deleted_at TIMESTAMP,
	PRIMARY KEY (id)
);

-- Start a transaction
DO $$ 
DECLARE
  list_id INT;
BEGIN
  FOR i IN 1..5 LOOP
    -- Insert into list_todo
    INSERT INTO list_todo (title, description)
    VALUES ('List ' || i, 'Description ' || i)
    RETURNING id INTO list_id;

    -- Insert into sublist_todo
    FOR j IN 1..3 LOOP
      INSERT INTO sublist_todo (list_id, title, description)
      VALUES (list_id, 'Sublist ' || i || '-' || j, 'Sublist Description ' || i || '-' || j);
    END LOOP;
  END LOOP;
END $$;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS list_todo;
DROP TABLE IF EXISTS sublist_todo;
-- +goose StatementEnd
