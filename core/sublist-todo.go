package core

import (
	"fmt"
	"log"
	"math"
	"strings"

	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/models"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/utils"
)

func (t *todoService) SearchSubListTodo(id, keyword string, page int, itemPerPage int) (*utils.PageResponse, error) {
	offset := (page - 1) * itemPerPage
	keyword = strings.ToLower(keyword)
	todos, err := t.repo.SearchSubListTodo(id, keyword, itemPerPage, offset)
	if err != nil {
		log.Println("failed to search sublist todos, err: ", err.Error())
		return nil, err
	}
	totalRow, err := t.repo.CountSubListTodo(id, keyword, itemPerPage, offset)
	if err != nil {
		log.Println("failed to get total row todos, err: ", err.Error())
	}
	numPage := int(math.Ceil(float64(totalRow) / float64(itemPerPage)))
	resp := utils.PageResponse{
		Total:        len(todos),
		Data:         todos,
		Page:         page,
		ItemPerPage:  itemPerPage,
		NumberOfPage: numPage,
	}
	return &resp, nil
}

func (t *todoService) GetDetailSublistTodo(id string) (*models.SublistTodo, error) {
	sublistTodo, err := t.repo.GetDetailSubListTodo(id)
	if err != nil {
		log.Println("failed to get detail sublist todo, err: ", err.Error())
		return nil, err
	}
	return sublistTodo, nil
}

func (t *todoService) CreateSublistTodo(req *models.SublistTodo) error {
	err := t.repo.CreateSublistTodo(req)
	return err
}

func (t *todoService) UpdateSublistTodo(req *models.SublistTodo) error {
	idStr := fmt.Sprint(req.ID)
	find, err := t.repo.GetDetailSubListTodo(idStr)
	if err != nil {
		log.Println("failed to get detail sublist todo, err: ", err.Error())
		return err
	}
	if find.ID == 0 {
		return fmt.Errorf("data not found")
	}

	err = t.repo.UpdateSublistTodo(req)
	return err
}

func (t *todoService) DeleteSublistTodo(req int) error {
	idStr := fmt.Sprint(req)
	find, err := t.repo.GetDetailSubListTodo(idStr)
	if err != nil {
		log.Println("failed to get detail sublist todo, err: ", err.Error())
		return err
	}
	if find.ID == 0 {
		return fmt.Errorf("data not found")
	}
	err = t.repo.DeleteSublistTodo(find)
	return err
}
