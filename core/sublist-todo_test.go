package core

import (
	"errors"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/jinzhu/gorm"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/models"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/repository"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/utils"
)

func Test_todoService_SearchSubListTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	//positive case
	pId := "1"
	pKeyword := "test"
	pPage := 1
	pItemPerPage := 10
	pOffset := (pPage - 1) * pItemPerPage
	expectResp := []models.SublistTodo{
		{
			Title:       "test",
			Description: "test",
		},
	}
	pTotalRow := int64(1)
	mockRepo.EXPECT().SearchSubListTodo("1", "test", pItemPerPage, pOffset).AnyTimes().Return(expectResp, nil)
	mockRepo.EXPECT().CountSubListTodo("1", "test", pItemPerPage, pOffset).AnyTimes().Return(pTotalRow, nil)

	//negative case - failed get search sublist todo
	nKeyword := "test2"
	mockRepo.EXPECT().SearchSubListTodo("1", "test2", pItemPerPage, pOffset).AnyTimes().Return(nil, errors.New("ErrSQL"))
	mockRepo.EXPECT().CountSubListTodo("1", "test2", pItemPerPage, pOffset).AnyTimes().Return(pTotalRow, nil)

	//negative case - failed get search sublist todo
	nExpectResp := []models.SublistTodo{
		{
			Title:       "test3",
			Description: "test3",
		},
	}
	mockRepo.EXPECT().SearchSubListTodo("1", "test3", pItemPerPage, pOffset).AnyTimes().Return(nExpectResp, nil)
	mockRepo.EXPECT().CountSubListTodo("1", "test3", pItemPerPage, pOffset).AnyTimes().Return(int64(0), errors.New("ErrSQL"))

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		id          string
		keyword     string
		page        int
		itemPerPage int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *utils.PageResponse
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{
				id:          pId,
				keyword:     pKeyword,
				page:        pPage,
				itemPerPage: pItemPerPage,
			},
			&utils.PageResponse{
				Total:        1,
				Data:         expectResp,
				Page:         1,
				ItemPerPage:  10,
				NumberOfPage: 1,
			},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{
				id:          pId,
				keyword:     nKeyword,
				page:        pPage,
				itemPerPage: pItemPerPage,
			},
			nil,
			true,
		},
		{
			"Test negative case 2",
			fields{repo: mockRepo},
			args{
				id:          pId,
				keyword:     "test3",
				page:        pPage,
				itemPerPage: pItemPerPage,
			},
			&utils.PageResponse{
				Total:        1,
				Data:         nExpectResp,
				Page:         1,
				ItemPerPage:  10,
				NumberOfPage: 0,
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			got, err := tr.SearchSubListTodo(tt.args.id, tt.args.keyword, tt.args.page, tt.args.itemPerPage)
			if (err != nil) != tt.wantErr {
				t.Errorf("todoService.SearchSubListTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("todoService.SearchSubListTodo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_GetDetailSublistTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pId := "1"
	expectResp := &models.SublistTodo{
		Title:       "test",
		Description: "test",
	}
	mockRepo.EXPECT().GetDetailSubListTodo("1").AnyTimes().Return(expectResp, nil)

	// negative case - failed get detail todo
	mockRepo.EXPECT().GetDetailSubListTodo("2").AnyTimes().Return(nil, errors.New("ErrSQL"))

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.SublistTodo
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{id: pId},
			expectResp,
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{id: "2"},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			got, err := tr.GetDetailSublistTodo(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("todoService.GetDetailSublistTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("todoService.GetDetailSublistTodo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_CreateSublistTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pReq := &models.SublistTodo{
		Title:       "test",
		Description: "test",
	}
	mockRepo.EXPECT().CreateSublistTodo(pReq).AnyTimes().Return(nil)

	// negative case - failed create sublist todo
	nReq := &models.SublistTodo{
		Title:       "test2",
		Description: "test2",
	}
	mockRepo.EXPECT().CreateSublistTodo(nReq).AnyTimes().Return(errors.New("ErrSQL"))

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		req *models.SublistTodo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{req: pReq},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{req: nReq},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			if err := tr.CreateSublistTodo(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("todoService.CreateSublistTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_UpdateSublistTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pReq := &models.SublistTodo{
		Model:       gorm.Model{ID: 1},
		Title:       "test",
		Description: "test",
	}
	mockRepo.EXPECT().GetDetailSubListTodo("1").AnyTimes().Return(pReq, nil)
	mockRepo.EXPECT().UpdateSublistTodo(pReq).AnyTimes().Return(nil)

	// negative case - failed update sublist todo
	nReq := &models.SublistTodo{
		Model:       gorm.Model{ID: 2},
		Title:       "test2",
		Description: "test2",
	}
	mockRepo.EXPECT().GetDetailSubListTodo("2").AnyTimes().Return(nReq, nil)
	mockRepo.EXPECT().UpdateSublistTodo(nReq).AnyTimes().Return(errors.New("ErrSQL"))

	// negative case - failed get detail sublist todo
	nReq2 := &models.SublistTodo{
		Model:       gorm.Model{ID: 3},
		Title:       "test3",
		Description: "test3",
	}
	mockRepo.EXPECT().GetDetailSubListTodo("3").AnyTimes().Return(nil, errors.New("ErrSQL"))

	// negative case - ID not found
	nReq3 := &models.SublistTodo{
		Model:       gorm.Model{ID: 33},
		Title:       "test33",
		Description: "test33",
	}
	mockRepo.EXPECT().GetDetailSubListTodo("33").AnyTimes().Return(&models.SublistTodo{}, nil)

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		req *models.SublistTodo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{req: pReq},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{req: nReq},
			true,
		},
		{
			"Test negative case 2",
			fields{repo: mockRepo},
			args{req: nReq2},
			true,
		},
		{
			"Test negative case 3",
			fields{repo: mockRepo},
			args{req: nReq3},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			if err := tr.UpdateSublistTodo(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("todoService.UpdateSublistTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_DeleteSublistTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pReq := &models.SublistTodo{
		Model: gorm.Model{
			ID: 1,
		},
		Title:       "test",
		Description: "test",
	}
	mockRepo.EXPECT().GetDetailSubListTodo("1").AnyTimes().Return(pReq, nil)
	mockRepo.EXPECT().DeleteSublistTodo(pReq).AnyTimes().Return(nil)

	// negative case - failed get detail list todo
	mockRepo.EXPECT().GetDetailSubListTodo("2").AnyTimes().Return(nil, errors.New("ErrSQL"))

	// negative case - failed delete list todo
	nReq2 := &models.SublistTodo{
		Model: gorm.Model{
			ID: 3,
		},
		Title:       "test3",
		Description: "test3",
	}
	mockRepo.EXPECT().GetDetailSubListTodo("3").AnyTimes().Return(nReq2, nil)
	mockRepo.EXPECT().DeleteSublistTodo(nReq2).AnyTimes().Return(errors.New("ErrSQL"))

	// negative case - failed delete list todo
	mockRepo.EXPECT().GetDetailSubListTodo("33").AnyTimes().Return(&models.SublistTodo{}, nil)

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		req int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{req: 1},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{req: 2},
			true,
		},
		{
			"Test negative case 2",
			fields{repo: mockRepo},
			args{req: 3},
			true,
		},
		{
			"Test negative case 3",
			fields{repo: mockRepo},
			args{req: 33},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			if err := tr.DeleteSublistTodo(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("todoService.DeleteSublistTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
