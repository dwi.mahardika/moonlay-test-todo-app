package core

import (
	"errors"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/jinzhu/gorm"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/models"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/repository"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/utils"
)

func Test_todoService_SearchListTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	//positive case
	pKeyword := "test"
	pPage := 1
	pItemPerPage := 10
	pOffset := (pPage - 1) * pItemPerPage
	expectResp := []models.ListTodo{
		{
			Title:        "test",
			Description:  "test",
			SublistTodos: nil,
		},
	}
	pTotalRow := int64(1)
	mockRepo.EXPECT().SearchListTodo("test", pItemPerPage, pOffset).AnyTimes().Return(expectResp, nil)
	mockRepo.EXPECT().CountListTodo("test", pItemPerPage, pOffset).AnyTimes().Return(pTotalRow, nil)

	//negative case - failed get search list todo
	nKeyword := "test2"
	mockRepo.EXPECT().SearchListTodo("test2", pItemPerPage, pOffset).AnyTimes().Return(nil, errors.New("ErrSQL"))
	mockRepo.EXPECT().CountListTodo("test2", pItemPerPage, pOffset).AnyTimes().Return(pTotalRow, nil)

	//negative case - failed get search list todo
	nExpectResp := []models.ListTodo{
		{
			Title:        "test3",
			Description:  "test3",
			SublistTodos: nil,
		},
	}
	mockRepo.EXPECT().SearchListTodo("test3", pItemPerPage, pOffset).AnyTimes().Return(nExpectResp, nil)
	mockRepo.EXPECT().CountListTodo("test3", pItemPerPage, pOffset).AnyTimes().Return(int64(0), errors.New("ErrSQL"))

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		keyword     string
		page        int
		itemPerPage int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *utils.PageResponse
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{
				keyword:     pKeyword,
				page:        pPage,
				itemPerPage: pItemPerPage,
			},
			&utils.PageResponse{
				Total:        1,
				Data:         expectResp,
				Page:         1,
				ItemPerPage:  10,
				NumberOfPage: 1,
			},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{
				keyword:     nKeyword,
				page:        pPage,
				itemPerPage: pItemPerPage,
			},
			nil,
			true,
		},
		{
			"Test negative case 2",
			fields{repo: mockRepo},
			args{
				keyword:     "test3",
				page:        pPage,
				itemPerPage: pItemPerPage,
			},
			&utils.PageResponse{
				Total:        1,
				Data:         nExpectResp,
				Page:         1,
				ItemPerPage:  10,
				NumberOfPage: 0,
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			got, err := tr.SearchListTodo(tt.args.keyword, tt.args.page, tt.args.itemPerPage)
			if (err != nil) != tt.wantErr {
				t.Errorf("todoService.SearchListTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("todoService.SearchListTodo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_GetDetailTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pId := "1"
	expectResp := &models.ListTodo{
		Title:        "test",
		Description:  "test",
		SublistTodos: nil,
	}
	mockRepo.EXPECT().GetDetailListTodo("1").AnyTimes().Return(expectResp, nil)

	// negative case - failed get detail todo
	mockRepo.EXPECT().GetDetailListTodo("2").AnyTimes().Return(nil, errors.New("ErrSQL"))
	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.ListTodo
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{id: pId},
			expectResp,
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{id: "2"},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			got, err := tr.GetDetailTodo(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("todoService.GetDetailTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("todoService.GetDetailTodo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_CreateListTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pReq := &models.ListTodo{
		Title:       "test",
		Description: "test",
	}
	mockRepo.EXPECT().CreateListTodo(pReq).AnyTimes().Return(nil)

	// negative case - failed create list todo
	nReq := &models.ListTodo{
		Title:       "test2",
		Description: "test2",
	}
	mockRepo.EXPECT().CreateListTodo(nReq).AnyTimes().Return(errors.New("ErrSQL"))

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		req *models.ListTodo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{req: pReq},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{req: nReq},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			if err := tr.CreateListTodo(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("todoService.CreateListTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_UpdateListTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pReq := &models.ListTodo{
		Model: gorm.Model{
			ID: 1,
		},
		Title:       "test",
		Description: "test",
	}
	mockRepo.EXPECT().GetDetailListTodo("1").AnyTimes().Return(pReq, nil)
	mockRepo.EXPECT().UpdateListTodo(pReq).AnyTimes().Return(nil)

	// negative case - failed get detail list todo
	nReq := &models.ListTodo{
		Model: gorm.Model{
			ID: 2,
		},
		Title:       "test2",
		Description: "test2",
	}
	mockRepo.EXPECT().GetDetailListTodo("2").AnyTimes().Return(nil, errors.New("ErrSQL"))

	// negative case - failed update list todo
	nReq2 := &models.ListTodo{
		Model: gorm.Model{
			ID: 3,
		},
		Title:       "test3",
		Description: "test3",
	}
	mockRepo.EXPECT().GetDetailListTodo("3").AnyTimes().Return(nReq2, nil)
	mockRepo.EXPECT().UpdateListTodo(nReq2).AnyTimes().Return(errors.New("ErrSQL"))

	// negative case ID == 0
	nReq3 := &models.ListTodo{
		Model: gorm.Model{
			ID: 33,
		},
		Title:       "test33",
		Description: "test33",
	}
	mockRepo.EXPECT().GetDetailListTodo("33").AnyTimes().Return(&models.ListTodo{}, nil)

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		req *models.ListTodo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{req: pReq},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{req: nReq},
			true,
		},
		{
			"Test negative case 2",
			fields{repo: mockRepo},
			args{req: nReq2},
			true,
		},
		{
			"Test negative case 3",
			fields{repo: mockRepo},
			args{req: nReq3},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			if err := tr.UpdateListTodo(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("todoService.UpdateListTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_DeleteListTodo(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockTodoRepo(ctrl)

	// positive case
	pReq := &models.ListTodo{
		Model: gorm.Model{
			ID: 1,
		},
		Title:       "test",
		Description: "test",
	}
	mockRepo.EXPECT().GetDetailListTodo("1").AnyTimes().Return(pReq, nil)
	mockRepo.EXPECT().DeleteListTodo(pReq).AnyTimes().Return(nil)

	// negative case - failed get detail list todo
	mockRepo.EXPECT().GetDetailListTodo("2").AnyTimes().Return(nil, errors.New("ErrSQL"))

	// negative case - failed delete list todo
	nReq2 := &models.ListTodo{
		Model: gorm.Model{
			ID: 3,
		},
		Title:       "test3",
		Description: "test3",
	}
	mockRepo.EXPECT().GetDetailListTodo("3").AnyTimes().Return(nReq2, nil)
	mockRepo.EXPECT().DeleteListTodo(nReq2).AnyTimes().Return(errors.New("ErrSQL"))

	// negative case ID == 0
	mockRepo.EXPECT().GetDetailListTodo("33").AnyTimes().Return(&models.ListTodo{}, nil)

	type fields struct {
		repo repository.TodoRepo
	}
	type args struct {
		req int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Test positive case",
			fields{repo: mockRepo},
			args{req: 1},
			false,
		},
		{
			"Test negative case",
			fields{repo: mockRepo},
			args{req: 2},
			true,
		},
		{
			"Test negative case 2",
			fields{repo: mockRepo},
			args{req: 3},
			true,
		},
		{
			"Test negative case 3",
			fields{repo: mockRepo},
			args{req: 33},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &todoService{
				repo: tt.fields.repo,
			}
			if err := tr.DeleteListTodo(tt.args.req); (err != nil) != tt.wantErr {
				t.Errorf("todoService.DeleteListTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
