package core

import (
	"fmt"
	"log"
	"math"
	"strings"

	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/models"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/utils"
)

func (t *todoService) SearchListTodo(keyword string, page int, itemPerPage int) (*utils.PageResponse, error) {
	offset := (page - 1) * itemPerPage
	keyword = strings.ToLower(keyword)
	todos, err := t.repo.SearchListTodo(keyword, itemPerPage, offset)
	if err != nil {
		log.Println("failed to get all todos, err: ", err.Error())
		return nil, err
	}
	totalRow, err := t.repo.CountListTodo(keyword, itemPerPage, offset)
	if err != nil {
		log.Println("failed to get total row todos, err: ", err.Error())
	}
	numPage := int(math.Ceil(float64(totalRow) / float64(itemPerPage)))
	resp := utils.PageResponse{
		Total:        len(todos),
		Data:         todos,
		Page:         page,
		ItemPerPage:  itemPerPage,
		NumberOfPage: numPage,
	}
	return &resp, nil
}

func (t *todoService) GetDetailTodo(id string) (*models.ListTodo, error) {
	todos, err := t.repo.GetDetailListTodo(id)
	if err != nil {
		log.Println("failed to get detail todos, err: ", err.Error())
		return nil, err
	}
	return todos, nil
}

func (t *todoService) CreateListTodo(req *models.ListTodo) error {
	err := t.repo.CreateListTodo(req)
	return err
}

func (t *todoService) UpdateListTodo(req *models.ListTodo) error {
	idStr := fmt.Sprint(req.ID)
	find, err := t.repo.GetDetailListTodo(idStr)
	if err != nil {
		log.Println("failed to get detail sublist todo, err: ", err.Error())
		return err
	}
	if find.ID == 0 {
		return fmt.Errorf("data not found")
	}
	err = t.repo.UpdateListTodo(req)
	return err
}

func (t *todoService) DeleteListTodo(req int) error {
	idStr := fmt.Sprint(req)
	find, err := t.repo.GetDetailListTodo(idStr)
	if err != nil {
		log.Println("failed to get detail sublist todo, err: ", err.Error())
		return err
	}
	if find.ID == 0 {
		return fmt.Errorf("data not found")
	}
	err = t.repo.DeleteListTodo(find)
	return err
}
