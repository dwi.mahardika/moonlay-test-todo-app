package core

import (
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/repository"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/service"
)

type todoService struct {
	repo repository.TodoRepo
}

func NewToDoService(repo repository.TodoRepo) service.ToDoListService {
	return &todoService{repo: repo}
}
