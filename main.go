package main

import (
	"os"

	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/server"

	"github.com/joho/godotenv"
)

func init() {
	godotenv.Load()
}

func main() {
	server := server.NewHTTPServer()
	host := os.Getenv("HOST")
	port := os.Getenv("PORT")
	server.Serve(host + ":" + port)
}
