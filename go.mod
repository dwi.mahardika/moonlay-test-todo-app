module gitlab.com/dwi.mahardika/moonlay-test-todo-app

go 1.18

require (
	github.com/golang/mock v1.6.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.5.1
	github.com/labstack/echo/v4 v4.11.3
	github.com/pressly/goose v2.7.0+incompatible
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
