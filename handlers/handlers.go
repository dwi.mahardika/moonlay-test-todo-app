package handlers

import "github.com/labstack/echo/v4"

type Handler interface {
	InitializeRoute(r *echo.Echo)
}

func RegisterHandler(r *echo.Echo, handlers ...Handler) {
	for _, h := range handlers {
		h.InitializeRoute(r)
	}
}
