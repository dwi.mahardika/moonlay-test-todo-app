package handlers

import (
	"net/http"
	"strconv"

	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/models"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/service"

	"github.com/labstack/echo/v4"
)

type todoHandler struct {
	svc service.ToDoListService
}

func NewTodoHandler(svc service.ToDoListService) Handler {
	return &todoHandler{
		svc: svc,
	}
}

func (t *todoHandler) InitializeRoute(e *echo.Echo) {
	todoGroup := e.Group("/todo")
	todoGroup.GET("", t.SearchListTodo)
	todoGroup.GET("/:id", t.GetDetailTodo)
	todoGroup.POST("", t.CreateListTodo)
	todoGroup.PUT("", t.UpdateListTodo)
	todoGroup.DELETE("/:id", t.DeleteListTodo)

	sublistTodoGroup := e.Group("/todo/sublist")
	sublistTodoGroup.GET("", t.SearchSublistTodo)
	sublistTodoGroup.GET("/:id", t.GetDetailTodo)
	sublistTodoGroup.POST("", t.CreateSublistTodo)
	sublistTodoGroup.PUT("", t.UpdateSublistTodo)
	sublistTodoGroup.DELETE("/:id", t.DeleteSublistTodo)
}

func (t *todoHandler) SearchListTodo(c echo.Context) error {
	q := c.QueryParam("keyword")
	pageParam := c.QueryParam("page")
	itemsPerPageParam := c.QueryParam("item_per_page")
	page := 1
	itemsPerPage := 10

	// Convert query parameters to appropriate types
	if pageParam != "" {
		page, _ = strconv.Atoi(pageParam)
	}
	if itemsPerPageParam != "" {
		itemsPerPage, _ = strconv.Atoi(itemsPerPageParam)
	}
	todos, err := t.svc.SearchListTodo(q, page, itemsPerPage)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}

	return c.JSON(http.StatusOK, todos)
}

func (t *todoHandler) GetDetailTodo(c echo.Context) error {
	id := c.Param("id")
	todos, err := t.svc.GetDetailTodo(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}

	return c.JSON(http.StatusOK, todos)
}

func (t *todoHandler) SearchSublistTodo(c echo.Context) error {
	q := c.QueryParam("keyword")
	pageParam := c.QueryParam("page")
	itemsPerPageParam := c.QueryParam("item_per_page")
	listID := c.QueryParam("list_id")
	//set default value
	page := 1
	itemsPerPage := 10

	if pageParam != "" {
		page, _ = strconv.Atoi(pageParam)
	}
	if itemsPerPageParam != "" {
		itemsPerPage, _ = strconv.Atoi(itemsPerPageParam)
	}
	todos, err := t.svc.SearchSubListTodo(listID, q, page, itemsPerPage)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}

	return c.JSON(http.StatusOK, todos)
}

func (t *todoHandler) GetDetailSublistTodo(c echo.Context) error {
	id := c.Param("id")
	todos, err := t.svc.GetDetailSublistTodo(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}

	return c.JSON(http.StatusOK, todos)
}

func (t *todoHandler) CreateListTodo(c echo.Context) error {
	todo := new(models.ListTodo)
	if err := c.Bind(todo); err != nil {
		return err
	}
	err := t.svc.CreateListTodo(todo)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return c.JSON(http.StatusCreated, todo)
}

func (t *todoHandler) UpdateListTodo(c echo.Context) error {
	todo := models.ListTodo{}
	if err := c.Bind(&todo); err != nil {
		return err
	}
	err := t.svc.UpdateListTodo(&todo)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return c.JSON(http.StatusCreated, todo)
}

func (t *todoHandler) DeleteListTodo(c echo.Context) error {
	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)
	err := t.svc.DeleteListTodo(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return c.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (t *todoHandler) CreateSublistTodo(c echo.Context) error {
	todo := new(models.SublistTodo)
	if err := c.Bind(todo); err != nil {
		return err
	}
	err := t.svc.CreateSublistTodo(todo)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return c.JSON(http.StatusCreated, todo)
}

func (t *todoHandler) UpdateSublistTodo(c echo.Context) error {
	todo := models.SublistTodo{}
	if err := c.Bind(&todo); err != nil {
		return err
	}
	err := t.svc.UpdateSublistTodo(&todo)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return c.JSON(http.StatusCreated, todo)
}

func (t *todoHandler) DeleteSublistTodo(c echo.Context) error {
	idStr := c.Param("id")
	id, _ := strconv.Atoi(idStr)
	err := t.svc.DeleteSublistTodo(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return c.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}
