package service

import (
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/models"
	"gitlab.com/dwi.mahardika/moonlay-test-todo-app/utils"
)

type ToDoListService interface {
	SearchListTodo(keyword string, page int, itemPerPage int) (*utils.PageResponse, error)
	GetDetailTodo(id string) (*models.ListTodo, error)
	SearchSubListTodo(id, keyword string, page int, itemPerPage int) (*utils.PageResponse, error)
	GetDetailSublistTodo(id string) (*models.SublistTodo, error)
	CreateListTodo(req *models.ListTodo) error
	UpdateListTodo(req *models.ListTodo) error
	DeleteListTodo(req int) error
	CreateSublistTodo(req *models.SublistTodo) error
	UpdateSublistTodo(req *models.SublistTodo) error
	DeleteSublistTodo(req int) error
}
