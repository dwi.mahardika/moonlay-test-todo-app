package models

import "github.com/jinzhu/gorm"

type ListTodo struct {
	gorm.Model
	Title        string        `json:"Title" validate:"required,max=100,alphanumunicode"`
	Description  string        `json:"Description" validate:"required,max=1000"`
	SublistTodos []SublistTodo `gorm:"foreignkey:ListID"`
}

func (ListTodo) TableName() string {
	return "list_todo"
}

type SublistTodo struct {
	gorm.Model
	ListID      int64
	Title       string `json:"Title" validate:"required,max=100,alphanumunicode"`
	Description string `json:"Description" validate:"required,max=1000"`
}

func (SublistTodo) TableName() string {
	return "sublist_todo"
}
