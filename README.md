# moonlay-test-todo-app

Copy .env.example to .env
```
cd moonlay-test-todo-app
cp .env.example .env
```

Modify .env to your env Then Run
```
go mod tidy
go run main.go
```

Or Run with Docker
```
sh build.sh
```
